# Data Logging Module #

Setup data logging in your Unity game in 3 steps.

### Step 1 ###

* Download Unity Package [here](https://bitbucket.org/neurorehablab/dataloggingmodule/downloads/DataLoggingModule.unitypackage)
* Import it in your game
* Drag the DataLogging prefab into your scene

### Step 2 ###

* Open the GameDataLogging script and edit the _header, _path and _filepath according to your needs (comments included on the script to assist you)


### Step 3 ###

* Invoke the CsvWrite function from somewhere in your game (see Test script for inspiration)
* Important: don't forget to close the _file at some point in your game ->happens by setting the StopLog variable to true

### And that's it! Happy Logging!! ###
 
* Feel free to open an issue with questions and/or suggestions